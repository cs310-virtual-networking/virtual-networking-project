# Virtual Networking for Security Analysis and Protocol Design

This project allows any user-space program to be connected to another user-space program via a virtual network rather than via the internet.
This allows for the controlling of network parameters such as network latency and throughput and also allows for complex models of
transmission.

The project consists of a number of components:
* VNIC (Virtual Network Interface Card) drivers
* Network simulator
  * This project can be connected to [ns-3](https://www.nsnam.org/) which, with sufficient knowledge,
    can be used to create almost any possible network configuration
  * The simulator also contains its own simple interface if you just want to define your own rules for forwarding packets
* Packet capture
  * This comes in the form of [PcapPlusPlus](https://pcapplusplus.github.io/), which is used to collect packets
    sent from the VNICs in order to pipe them through the network simulator. A version of PcapPlusPlus has been included in
    this repository which contains a couple of tweaks to make the project work.

# Terminology
* VNIC - A Virtual Network Interface Card, used for routing packets within a computer.
* Network Namespace - An isolated network environment, which can only use a specific
  set of network interfaces.

# Getting Started

Installing some of the components is not the most straightforward. This document will guide you through installation of each component.
A fair amount of compiling is required, so there will be plenty of time for coffee breaks during the install process.

This guide will contain the basic instructions to get things running. Each component that is required is included as a submodule of
this repo. The individual repos will have more information about each component if you want to learn more.

This project required a number of Git repositories for different bits of functionality.
This repo contains **git submodules** to link together these repositories, and provide
versions of each repo which should work together. If you want to understand more about
git submodules, there is a link here:  
https://git-scm.com/book/en/v2/Git-Tools-Submodules

> :warning: **If you checkout each repo manually** you may get incompatible versions.
Please use this repo and the git submodules to ensure you have the correct versions.

## Prerequisites

### System

Recommended to run this project on a **Virtual Machine** since you will be loading Kernel modules.   

I highly recommend **Qemu/KVM** to run this project since it fully supports loading
kernel modules. Issues have occurred when testing on other virtualisation software
such as VMware.  
For a simple-to-use graphical interface to Qemu, I recommend **virt-manager**.

Recommended system:  
* **Ubuntu 20.04** was used during development. Theoretically any modern Linux distro should work, but only Ubuntu 20.04 has been thoroughly tested.
* 2 cores and 4GB of RAM should be sufficient, but if you are building on the VM, the
more cores you can give it, the quicker it will be.
* (It is assumed you can download and install a VM of Ubuntu 20.04)
* Note: Your VM must be able to open multiple terminals simultaneously

**Step 1**  
Install your VM. Ubuntu 20.04 desktop or server versions are both fine.

> :warning: One of the example programs uses a video feed. If you want to be able
to run this example application, your VM will need a GUI. The rest of the project
will run fine on a headless installation.

You will need to clone this repo, but **before you clone**  you will need to
have ssh keys for your VM connected to a GitHub account **AND** a GitLab account.

If you don't have accounts on both of these platforms, please create one.

**Step 2**  
Add your SSH keys to GitLab and GitHub.

This requirement is due to the fact that some of the open source projects which
are part of this project use GitHub and some use GitLab.

Follow these instructions for adding a key to your GitHub account:
[Adding a new SSH key to your GitHub account](https://docs.github.com/en/github/authenticating-to-github/adding-a-new-ssh-key-to-your-github-account)

Follow these instructions for adding a key to your GitLab account:
[GitLab and SSH keys](https://docs.gitlab.com/ee/ssh/)

This is simply to be able to clone the necessary repos.

**Step 4**  
You will need `git` installed on the VM in order to clone the repository.
```
$ sudo apt install -y git
```

**Step 3**  
Clone this repository onto the VM

```
$ git clone --recurse-submodules git@gitlab.com:cs310-virtual-networking/virtual-networking-project.git
```

Now change directory into the git repo
```
$ cd virtual-networking-project/
```
performing an `ls` should show you the following files:
```
.
├── cs310-simulator-interface/
├── cs310-testing/
├── cs310-video-stream/
├── cs310-vnics/
├── ns-3-dev/
├── PcapPlusPlus/
└── README.md
```
These folders should each contain the contents of their respective projects.
If they are empty, it is likely that you forgot to pass the `--recurse-submodules`
flag when cloning. To fix this, run
```
$ git submodule init
$ git submodule update
```
If there are ever any changes to this repo, meaning that a different commit is tracked,
you can update all of the submodules with a
```
$ git pull --recurse-submodules
```

# Installing dependencies
**Step 4**
You will need the following dependencies. The commands to acquire these on an Ubuntu
machine with `apt` are shown for each one.

(This list includes all dependencies for all submodules)

* gcc
* g++
* make
* jq
* ifconfig (net-tools)
* libpcap developers pack (libpcap-dev)
```
$ sudo apt install -y gcc g++ make jq net-tools libpcap-dev
```


# Setting up cs310-vnics

## Configuration

Enter the vnic directory
```
$ cd cs310-vnics/
```
Copy the `example_config/vnic_config.json` into the current folder. This must be in the
root of the `cs310-vnics` project in order to configure the setup of the vnics properly.
```
$ cp example_config/vnic_config.json .
```
You can take a look at the `vnic_config.json` file to see how the vnics are configured.
```
$ cat vnic_config.json
```
If you are just running the example programs, this file already contains the correct
configuration for the VNICs, so do not edit this file. If you are running your
own programs over the network simulator, you may want to alter the configuration
to work with your own programs.

## Building
To make the vnic module, type:
```
$ make
```
You may see some warnings, however the project should compile successfully.

## Running
To run the vnic module, type:
```
$ sudo ./load_vnics
```
This will load the vnics module into your kernel and setup the vnics defined in
`vnic_config.json`.  
The `load_vnics` script will print out each command that it executes to the terminal,
prefixed with a `#` (since the commands are run as root).

## Verifying that it is running

### Check that the kernel module is running
```
$ lsmod | grep vnic
```
If the module is running you should see an output like the following:
```
vnic                   16384  0
```
If the module is not running, nothing will be displayed.

### Check which network namespaces are up
To list the current network namespaces:
```
$ ip netns list
```
To run a command within a network namespace:
```
$ sudo ip netns exec <namespace> <command>
```
For example, to run `ifconfig` to check the details of vnics within the namespace
`space1`, run:
```
$ sudo ip netns exec space1 ifconfig
```

## Stopping
To stop execution:
```
$ sudo ./unload_vnics
```

# Setting up PcapPlusPlus
Change directory into the PcapPlusPlus directory.
```
$ cd PcapPlusPlus/
```

## Configuration

Configure PcapPlusPlus with the defaults for Linux.

```
$ ./configure-linux.sh --default
```

## Building

The `-jN` flag specifies the number of concurrent jobs to run during the make.
Set this to the number of cores that you gave to your VM.
```
$ make all -j2
```
This can take a few minutes.

## Installing

Once built, install with
```
$ sudo make install
```
This will place binaries in `/usr/local/bin` and headers in `/usr/local/include`.

## Uninstalling
You can uninstall at any point by navigating to the PcapPlusPlus directory and running:
```
$ sudo make uninstall
```

# cs310-simulator-interface
> :warning: PcapPlusPlus must be installed to use

This component is a simple interface for defining your own packet forwarding rules.
This is **An ALTERNATIVE** to using ns-3. When a packet arrives, it is stored
alongside the time at which it should be sent across the network. When this
time is reached, the packet will be forwarded.

Change directory into the cs310-simulator-interface directory.
```
$ cd cs310-simulator-interface
```

## Building

Simply run
```
$ make
```
to build.

## Running

There is a script which will run the output of this file inside a the Network Namespace
`space1`:
```
$ sudo ./run-capture
```

## Stopping  
Press `Ctrl + C` to stop the program.

# Setting up ns-3 (ns-3-dev)

> :warning: For ns-3 to build the required modules, PcapPlusPlus must already
be installed.


Change directory into the ns-3 directory.
```
$ cd ns-3-dev/
```

`waf` is the build tool used by ns-3. This is a python program included in the root
directory of the project. This is run much like a bash script from the command line.

## Configuration

Configure ns-3 with the following options:
```
$ ./waf configure --enable-examples --enable-sudo --build-profile=debug
```
This will enable examples, allow calls which require root access to be run, and
enables debug mode.

Allowed build profiles are:
* debug
* optimized
* release

When you have run the above command, ns-3 will list which components are enabled or
disabled. Make sure that `Real Application` is listed as `enabled`.

Ns-3 contains modules, each of which provides some functionality for the network 
simulator. A module was developed for ns-3 as part of this project called
`Real Application`. The source code for this module is located in:
`src/real-application/` within ns-3. Most of the code is in the 
`src/real-application/model/` directory, and an example script demonstrating how
to set up the real application is available in
`src/real-application/examples/`. We will get onto what is being done in the
example and how to run it soon.


## Building

Once configured, run:
```
$ ./waf build
```
Since the flag `--enable-sudo` will have been passed (assuming you're following this
setup guide), `waf` will ask for the sudo password at the end of the build.

Building takes around 10 minutes, so now is the perfect time for a coffee break.

## Running

Ns-3 has a powerful scripting interface. If you want to be able to write your own
scripts there is a very comprehensive
[tutorial](https://www.nsnam.org/docs/tutorial/html/)
on the nsnam website. This will require a lot of reading to become comfortable with
ns-3. If you just want to run the example scripts provided as part of the 

The example is called `real-application-example`.

Not everything is configured yet, so when you run, there **WILL BE AN ERROR** with
the message:
"Unable to find network interface device vnic0". Don't panic. This is expected.  
Run the following command to run the application using the following command
```
$ ./waf --run real-application-example
```
You should receive the above mentioned error.

In order to connect to the vnics, you need to ensure that the `cs310-vnics`
submodule is running (see the `cs310-vnics` section) and you need to launch
ns-3 within the network namespace where `vnic0` and `vnic1` are located.

`vnic0` and `vnic1` belong to Namespace `space1`. Therefore to run ns3, use the command:
```
$ sudo ip netns exec space1 ./waf --run real-application-example
```

## Stopping

Press `Ctrl + C` to stop ns-3.

# System architecture
To run the project it is important that you have at least a basic understanding
of what is being done.

* Programs are connected to the network simulator ns-3.
* Each program will run in its Network Namespace.
* Ns-3 will also run in a Network Namespace: `space1`
* All other programs will run in a Network Namespace `spaceN` where `N>1`
* The VNICs connect the network namespaces together.

## What needs to be run

To run the project, you MUST run the submodule:
* `cs310-vnics`

to move packets around the system.

You MUST run exactly ONE of the following network simulators:
* `ns-3-dev`
* `cs310-simulator-interface`

in order to manage the transmission of packets. When a packet is sent, the
VNICs will send the packet to the network simulator, and the network simulator
will decide when to forward the packet on to its destination.

In order to run EITHER simulator you MUST have installed:
* `PcapPlusPlus`

Since this provides the capability for the network simulator to capture packets
from the VNICs.

You MAY test the capability of the network simulator with programs in the following
repos:
* `cs310-testing`
* `cs310-video-stream`

These repos contain programs and bash scripts to easily run them over the network
simulator.

You may also run other programs over the network simulator such as:
* ping

## Configuration conventions

The following configuration conventions are important to understand if you want
to tweak the project to your needs, for example changing IP addresses of VNICs
or adding and removing network namespaces.

You will not need this information just to run the examples, however it is very
useful to be able to understand the project.

The configuration of the VNICs is set within `cs310-vnics/vnic_config.json`.
This defines the parameters used to initialise the VNICs. Please take a look at
this file to see what it set.

**id**  
* Sets the number `vnic<id>` when creating an instance of a `vnic`  
* When you query `ifconfig` the name of the interface will show your its id
* `id`s MUST be assigned starting from 0, and incrementing 1 at a time

**namespace**
* Sets the number of the namespace that the vnic belongs to
* e.g. if namespace is set to `3`, the vnic will appear in namespace `space3`
* Namespaces MUST be assigned **starting from 1** and incrementing 1 at a time

**Network Simulator Section**  
The network simulator has some specific rules for how it must be configured.
* Namespace `1` belongs to the network simulator
* `vnic0` and `vnic1` are the receiving and sending VNICs for the network
  simulator. These VNICs MUST be assigned to namespace `1`.

The reason for VNICs starting at 0 and Namespaces starting at 1 is because the
Network Simulator uses 2 VNICs, and assuming you assign one VNIC per Namespace
for all remaining Namespaces, the VNIC and the Namespace will have the same number.

**ip_addr**,  
**subnet_mask**,  
**gateway**
* Define parameters for the network they belong to
* For the network simulator (Namespace `1`) section these settings are
  unimportant, since they are not used for packet routing or to edit the
  data within a packet.
* They MUST be valid addresses for EVERY network card (including `vnic0` and `vnic1`).
  This is because the
  VNICs are registered as ethernet devices and therefore must have a valid
  address.
* Each `ip_addr` must be UNIQUE. Overlapping private networks are currently not
  supported.

**Configuring the network simulator (cs310-simulator-interface)**  
No specific setup is required for `cs310-simulator-interface` to be able to forward
packets. Just ensure that the rules stated above are followed.

**Configuring the network simulator (ns-3-dev)**  
Configuration is performed within the script that you write. The following parameters
MUST be assigned and MUST match the values given provided to the VNICs:
* IP Address
* Subnet Mask
* MAC address

Make sure to specify:
* Port numbers  

to listen for incoming traffic on.

Please look at the example script within `ns-3-dev/src/real-application/examples/`
to see how to set each of these parameters.

# Running the examples

Assuming you've managed to follow all of the configuration, build and install
steps successfully, you should now be ready to try out some examples.

## Running text messaging over ns-3

1. Launch the VNICs as described in the `cs310-vnics` section.
2. Launch ns-3 as described in the `ns-3-dev` section.
3. Open 2 more terminals and navigate to `cs310-testing/` directory.
4. Run the following commands to launch the server and the client text applications:
```
$ sudo ./start-server
```
```
$ sudo ./start-client
```
You should be able to send messages between the server and the client.

Ns-3 will log the messages being sent.

To see the data going through the VNICs, open another terminal and run
```
$ dmesg -wH
```

## Running video stream over cs310-simulator-interface
> :warning: **Note**: You will need a webcam connected to the VM you are using
to be able to run the video stream server and client.

There are a few requirements to install. First:
* pip3 (python3-pip)
```
$ sudo apt install -y python3-pip
```

Make sure pip is up to date:
```
$ python3 -m pip install --upgrade pip
```

You will then need to install the following requirements for the **root** user
using the pip package manager:
* cv2 (opencv-python)
```
$ sudo pip3 install opencv-python numpy
```

OpenCV has some requirements which need satisfying:
```
$ sudo apt install -y ffmpeg libsm6 libxext6
```

1. Launch the VNICs as described in the `cs310-vnics` section.
2. Launch `cs310-simulator-interface` as described in its section.
3. Open 2 more terminals and navigate to the `cs310-simulator-interface/` directory
4. Run the following commands to launch the server and client video stream
   applications:
```
$ sudo ./run_server
```
```
$ sudo ./run_client
```
Two popup windows will appear, labelled with "Sender" and "Receiver"

You should see a significant (and intentional) delay between the sender and
receiver displaying the video feed.

# Running your own programs

To run your own programs, you may want to change the configuration of the VNICs
using the `vnic_config.json` file in `cs310-vnics` repo.

Don't forget that if you change the configuration file, then if you are using ns-3
you will need to tweak the example file to match the configuration that you have
created.

You will need to run your program in a network namespace using the following
command:

```
$ sudo ip netns exec <namespace> <command>
```
Remember that the namespace `space1` is used by the network simulator, so
your own programs will need to be run on `space<N>` where `N >= 2`.

## Example of running another program

If you wanted to run ping using the default network interface, I know that in the
example setup namespace `space2` has ip address `192.168.0.30` and `space3` has
ip address `192.168.0.40`.

After launching the VNICs and the network simulator, you would then be able to run
ping by using the command:
```
$ sudo ip netns exec space2 ping 192.168.0.40
```
Something to keep in mind is that the root will be running the program, so
you will have to make sure any configuration is available when running as **sudo**.
An example of this is if using Python, you may need to:
```
$ sudo pip install <requirement>
```